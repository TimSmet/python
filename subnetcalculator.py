def ask_for_number_sequence(message):
    print(message)
    ip_address = input()
    return [int(elem) for elem in ip_address.split(".")]

def is_valid_ip_address(numberlist):
    if len(numberlist) != 4:
        return False
    for number in numberlist:
      if number < 0 or number > 255:
        return False
    return True

def is_valid_netmask(numberlist):
    if len(numberlist) != 4:
        return False
    binary_netmask = ""
    for number in numberlist:
        binary_netmask  = binary_netmask  + f"{number:08b}"
    checking_ones = True

    for number in binary_netmask:
        if number == "0" and checking_ones == True:
            checking_ones = False
        if number == "0" and checking_ones ==False:
            checking_ones = False
        if number == "1" and checking_ones ==False:
            return False
    return True

def one_bits_in_netmask(numberlist):
    binary_netmask = ""
    counter = 0
    for number in numberlist:
        binary_netmask  = binary_netmask  + f"{number:08b}"

    for number in binary_netmask:
        if number == "1":
            counter +=1
    return counter

def apply_network_mask(host_address ,netmask ):
        deel_1= host_address[0] & netmask[0]
        deel_2= host_address[1] & netmask[1]
        deel_3= host_address[2] & netmask[2]
        deel_4= host_address[3] & netmask[3]
        return str(deel_1)+"."+ str(deel_2)+"."+str(deel_3)+"."+str(deel_4)

def netmask_to_wildcard_mask(netmask):
        wildcard_mask = []
        wildcard_deel_1=""
        wildcard_deel_2=""
        wildcard_deel_3=""
        wildcard_deel_4=""
        for bit in f"{netmask[0]:08b}":
            if bit == "0":
                wildcard_deel_1 += "1"
            else:
                wildcard_deel_1 += "0"
        for bit in f"{netmask[1]:08b}":
             if bit == "0":
                wildcard_deel_2 += "1"
             else:
                wildcard_deel_2 += "0"

        for bit in f"{netmask[2]:08b}":
             if bit == "0":
                 wildcard_deel_3 += "1"
             else:
                 wildcard_deel_3 += "0" 
        for bit in f"{netmask[3]:08b}":
             if bit == "0":
                 wildcard_deel_4 += "1"
             else:
                 wildcard_deel_4 += "0"
        wildcard_mask = str(int(wildcard_deel_1,2))+"."+str(int(wildcard_deel_2,2))+"."+ str(int(wildcard_deel_3,2))+"."+ str(int(wildcard_deel_4,2))
        return [int(elem) for elem in wildcard_mask.split(".")]

def get_broadcast_address(network_address, wildcard_mask):
        deel_1= network_address[0] | wildcard_mask[0]
        deel_2= network_address[1] | wildcard_mask[1]
        deel_3= network_address[2] | wildcard_mask[2]
        deel_4= network_address[3] | wildcard_mask[3]
        broadcast = str(deel_1)+"."+ str(deel_2)+"."+str(deel_3)+"."+str(deel_4)       
        return [int(elem) for elem in broadcast.split(".")]

def prefix_length_to_max_hosts(number):
   result = pow(number,2)-2
   return result

ip = ask_for_number_sequence("Wat is het IP-adres?")
mask = ask_for_number_sequence("Wat is het subnetmasker?")

iptrue = is_valid_ip_address(ip)

masktrue = is_valid_netmask(mask)

if masktrue ==True and iptrue == True:
    print("IP-adres en subnetmasker zijn geldig.")

else:
    print("IP-adres en/of subnetmasker is ongeldig.")
    exit()

subnet_mask_length = one_bits_in_netmask(mask)
print("De lengte van het subnetmasker is " + str(one_bits_in_netmask(mask)))

print("Het adres van het subnet is " + str(apply_network_mask(ip,mask)))

wild = netmask_to_wildcard_mask(mask)
print("Het wildcardmasker is " + str(netmask_to_wildcard_mask(mask)))
print("Het broadcastadres is " + str(get_broadcast_address(ip,wild)))

print("Het maximaal aantal hosts op dit subnet is " + str(prefix_length_to_max_hosts(subnet_mask_length)))

